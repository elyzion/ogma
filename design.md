# MVP

- On one device, can locally
    - Can do video chat
        - Audio channel
        - Video channel
- Can connect peer to peer 
    - Can do video chat
        - Audio channel
        - Video channel
    - Can share data (messages and files) file 
        -data channel
- Can use overlay/filters to annotate an image (don't save/load for the moment)
- Bonus: interactive text/media session
  - Select correct case for word in sentence
  - Select corrent word from list
  - Select correct sentence 

# Other stuff

- html/css transformations (webpack). This is a different issue as I'm not sure how it will interact with the rust stuff.
