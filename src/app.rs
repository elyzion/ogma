use js_sys::Promise;
use log::*;
use wasm_bindgen::{prelude::*, JsCast};
use web_sys::{
    HtmlMediaElement, HtmlVideoElement, MediaDevices, MediaStream, MediaStreamConstraints,
    MediaStreamTrack, RtcOfferOptions, RtcPeerConnection,
};
use yew::{
    prelude::*,
    services::storage::{Area, StorageService},
};

use std::future::Future;

use crate::error::MediaResolutionError;

// Large TODO:
// - https://webrtc.github.io/samples/src/content/peerconnection/pc1/
// - Simple Signalling/discovery server
// - STUN/TURN via coturn
// - https://webrtc.github.io/samples/src/content/datachannel/filetransfer/
// - https://webrtc.github.io/samples/src/content/datachannel/messaging/

/// This method processes a Future that returns a message and sends it back to the component's
/// loop.
/// Standard piece of boiler plate code taken from Yew.
///
/// # Panics
/// If the future panics, then the promise will not resolve, and will leak.
pub fn send_future<COMP: Component, F>(link: &ComponentLink<COMP>, future: F)
where
    F: Future<Output = COMP::Message> + 'static,
{
    use wasm_bindgen_futures::future_to_promise;

    let link = link.clone();
    let js_future = async move {
        link.send_message(future.await);
        Ok(JsValue::NULL)
    };

    let _ = future_to_promise(js_future);
}

/// The possible states a fetch request can be in.
pub enum MediaState<T> {
    NotResolving,
    Resolving,
    Success(T),
    Failed(MediaResolutionError),
}

// Note: you should never use expect like this unless you are prototyping.
// For a real product you would use something like failure: https://github.com/withoutboats/failure
// TODO: would be better to switch to MediaTrackConstraints :)
async fn resolve_media() -> Result<MediaStream, MediaResolutionError> {
    let window = web_sys::window().expect("Could not extract window object");
    info!("window! {:?}", window);
    let navigator = window.navigator();
    info!("navigator! {:?}", navigator);

    // TODO: https://webrtc.org/getting-started/media-devices#media_constraints
    // - Echo cancellation
    // - Selecting the device to use
    let mut constraints = MediaStreamConstraints::new();
    constraints.audio(&JsValue::from_str("{'echoCancellation': true}"));
    constraints.video(&true.into());
    info!("constraints! {:#?}", constraints);
    let media_devices: MediaDevices = navigator
        .media_devices()
        .expect("Could not get media devices");
    info!("media_devices! {:#?}", media_devices);
    let media_stream_promise: Promise = media_devices
        .get_user_media_with_constraints(&constraints)
        .expect("Could not get user media");
    info!("media_stream {:?}", media_stream_promise);
    let ok = wasm_bindgen_futures::JsFuture::from(media_stream_promise)
        .await
        .expect("Could not get media stream");
    info!("ok!");

    assert!(ok.is_instance_of::<MediaStream>());
    let ms: MediaStream = ok.dyn_into().expect("Could not cast into MediaStream");
    Ok(ms)
}

#[allow(dead_code)]
pub struct App {
    link: ComponentLink<Self>,
    storage: StorageService,
}

#[allow(dead_code)]
pub enum Msg {
    Call,
    Hangup,
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let storage = StorageService::new(Area::Local).unwrap();
        App { link, storage }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Call => {}
            Msg::Hangup => {}
        }
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        info!("rendered!");
        html! {
            <div class="container-fluid">
                <div class="row">
                <div class="col">
                <Video/>
                </div>
                </div>
                <div class="row">
                <div class="col"></div>
                <div class="col"></div>
                </div>
                </div>
        }
    }
}

pub enum VideoMessage {
    SetMediaResolutionState(MediaState<MediaStream>),
    Start,
    Call,
    Hangup,
}

pub struct Video {
    media_state: MediaState<MediaStream>,
    link: ComponentLink<Self>,
    local_node_ref: NodeRef,
    remote_node_ref: NodeRef,
}

impl Component for Video {
    type Message = VideoMessage;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let local_node_ref = NodeRef::default();
        let remote_node_ref = NodeRef::default();
        Video {
            media_state: MediaState::NotResolving,
            link,
            local_node_ref,
            remote_node_ref,
        }
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            VideoMessage::Start => {
                let future = async {
                    match resolve_media().await {
                        Ok(media_stream) => {
                            VideoMessage::SetMediaResolutionState(MediaState::Success(media_stream))
                        }
                        Err(err) => VideoMessage::SetMediaResolutionState(MediaState::Failed(err)),
                    }
                };
                send_future(&self.link, future);
                self.link
                    .send_message(VideoMessage::SetMediaResolutionState(MediaState::Resolving));
                false
            }
            VideoMessage::Call => {
                let mut offer = RtcOfferOptions::new();
                offer.offer_to_receive_audio(true);
                offer.offer_to_receive_video(true);
                match &self.media_state {
                    MediaState::Success(media_stream) => {
                        let vt = media_stream.get_video_tracks();
                        let at = media_stream.get_audio_tracks();
                        debug!("Local VT: {:#?}", vt);
                        debug!("Local VT: {:#?}", at);
                        //let local = RtcPeerConnection::new().unwrap();
                        //local.set_onicecandidate(Some(|event| true));

                        true
                    }
                    _ => false,
                }
                // Calling needs a ton of stuff;
                // Discovery, which needs a simple server as per: https://www.html5rocks.com/en/tutorials/webrtc/infrastructure/#what-is-signaling
                // STUN/TURN as per https://www.html5rocks.com/en/tutorials/webrtc/infrastructure/#after-signaling-using-ice-to-cope-with-nats-and-firewalls
                //
            }
            VideoMessage::Hangup => match &self.media_state {
                MediaState::Success(media_stream) => {
                    // Could also get do a `HtmlMediaElement::from(hve).src_object()`
                    // to get the media_stream to operate on.
                    let _ = media_stream
                        .get_tracks()
                        .iter()
                        .filter_map(|v: JsValue| v.dyn_into::<MediaStreamTrack>().ok())
                        .for_each(|ms: MediaStreamTrack| {
                            if ms.enabled() {
                                ms.stop()
                            }
                        });
                    let hve: HtmlVideoElement = self.local_node_ref.cast().unwrap();
                    HtmlMediaElement::from(hve).set_src_object(None);
                    true
                }
                _ => {
                    info!("Media stream is not present");
                    false
                }
            },
            VideoMessage::SetMediaResolutionState(media_state) => {
                self.media_state = media_state;
                match &self.media_state {
                    MediaState::Success(media_stream) => {
                        let hve: HtmlVideoElement = self.local_node_ref.cast().unwrap();
                        HtmlMediaElement::from(hve).set_src_object(Some(media_stream));
                        true
                    }
                    _ => false,
                }
            }
        }
    }

    fn view(&self) -> Html {
        info!("rendered!");
        // TODO: Either add start/stop controls here and figure out how to select only the video
        // element via local_node_ref and web_sys, or wrap this in another component (ie, VideoInterface
        // -> { Video, VideoControlPane }.
        // Seems like I can do it here and just add a separate ref per node that I need to interact
        // with :)
        #[rustfmt::skip]
        html! {
            <div>
                <div class="row">
                    <div class="col">
                        <video autoplay=true playsinline=true controls=false ref=self.local_node_ref.clone()/>
                    </div>
                    <div class="col">
                        <video autoplay=true playsinline=true controls=false ref=self.remote_node_ref.clone()/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <button type="button" class="btn btn-primary" onclick=self.link.callback(|_| VideoMessage::Start)>{ "Start" }</button>
                    </div>
                    <div class="col-4">
                       <input type="text" name="remote"/>
                       <button type="button" class="btn btn-primary" onclick=self.link.callback(|_| VideoMessage::Call)>{ "Call" }</button>
                    </div>
                    <div class="col-4">
                        <button type="button" class="btn btn-danger" onclick=self.link.callback(|_| VideoMessage::Hangup)>{ "Hangup" }</button>
                    </div>
                </div>                
            </div>
        }
    }
}
