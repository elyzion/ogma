use std::error::Error;
use std::fmt::{Debug, Display, Formatter};
use wasm_bindgen::JsValue;

/// Something wrong has occurred while fetching an external resource.
#[derive(Debug, Clone, PartialEq)]
pub struct MediaResolutionError {
    err: JsValue,
}
impl Display for MediaResolutionError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(&self.err, f)
    }
}
impl Error for MediaResolutionError {}

impl From<JsValue> for MediaResolutionError {
    fn from(value: JsValue) -> Self {
        MediaResolutionError { err: value }
    }
}
